﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class AlmacenMap:EntityTypeConfiguration<Almacen>
    {
       public AlmacenMap()
       {
           this.HasKey(p => p.IdAlmacen);
           this.Property(p => p.IdAlmacen).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.HasRequired(p => p.Producto).WithMany().HasForeignKey(p => p.IdProductoFk).WillCascadeOnDelete(true);
           this.HasRequired(p => p.Movimientos).WithMany().HasForeignKey(p => p.IdMovimientoFk).WillCascadeOnDelete(true);

           this.ToTable("Almacen");
       }
    }
}
