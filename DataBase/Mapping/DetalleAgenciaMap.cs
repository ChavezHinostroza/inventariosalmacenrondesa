﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataBase.Mapping
{
  public class DetalleAgenciaMap:EntityTypeConfiguration<DetalleAgencia>
    {
      public DetalleAgenciaMap()
      {
          this.HasKey(p => new { p.IdArea, p.IdAgencia });

          this.ToTable("DetalleAgencia");

          HasRequired(p => p.Area)
              .WithMany()
              .HasForeignKey(p => p.IdArea)
              .WillCascadeOnDelete(false);

          HasRequired(p => p.Agencia)
              .WithMany(p=>p.DetallesAgencias)
              .HasForeignKey(p => p.IdAgencia)
              .WillCascadeOnDelete(true);

      }
    }
}
