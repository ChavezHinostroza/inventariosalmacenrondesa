﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DataBase;

namespace GenerateDB
{
    class Program
    {
        static void Main(string[] args)
        {
            //var Proveedor01 = new Proveedor()
            //{
            //    Representante = "Luis Mejia",
            //    Correo = "coop@hotmail.com",
            //    RazonSocial = "Coop.Rondesa",
            //    Telefono="356673",
            //    RUC="12345678912"
            //};

            var area01 = new Area()
            {
                Nombre = "Sistemas",
                CodigoArea="SI"
            };

            var _context = new RondesaContex();
            Console.WriteLine("Creando Base De Datos");
            _context.Areas.Add(area01);            
            _context.SaveChanges();

            Console.WriteLine("Base de datos creada con exito!!");
            Console.ReadLine();
        }
    }
}
