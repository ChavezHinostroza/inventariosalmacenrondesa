﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;

namespace Interfaces
{
   public interface InterfaceTipoProducto
    {
        List<TipoProducto> All();
        List<TipoProducto> AllTipoProducto(string criterio);
        void Store(TipoProducto tipoProducto);
        TipoProducto Find(int id);
        void Update(TipoProducto tipoProducto);
        void Delete(int id);
    }
}
