﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Repository;
using Interfaces;
using Validator.BienesValidator;

namespace SGRondesa.Controllers
{
    public class BienesController : Controller
    {
        private InterfaceBien repoBienes;
        private InterfaceProducto repoProducto;
        private InterfaceAgencia repoAgencia;
        private InterfaceArea repoArea;
        private BienesValidator validator;
        private InterfaceDepreciacion repoDepreciacion;

        public BienesController(InterfaceBien repoBienes, InterfaceProducto repoProducto, InterfaceAgencia repoAgencia, InterfaceArea repoArea, BienesValidator validator, InterfaceDepreciacion repoDepreciacion)
        {
            this.repoBienes = repoBienes;
            this.repoProducto = repoProducto;
            this.repoAgencia = repoAgencia;
            this.repoArea = repoArea;
            this.repoDepreciacion = repoDepreciacion;
            this.validator = validator;
        }

        private void TableProductosYAgencia()
        {
            //var producto = repoProducto.AllProducto("");
            //ViewData["IdProductofk"] = new SelectList(producto, "IdProducto", "Nombre");

            var agencia = repoAgencia.All();
            ViewData["IdAgencia"] = new SelectList(agencia, "IdAgencia", "Nombre");

            var area = repoArea.AllArea("");
            ViewData["IdAreafk"] = new SelectList(area, "IdArea", "Nombre");

            var depreciacion = repoDepreciacion.AllDepreciacion("");
            ViewData["IdDepreciacionFk"] = new SelectList(depreciacion, "IdDepreciacion", "Porcentaje");

        }

        [HttpGet]
        public ViewResult Index(DateTime? fecha1, DateTime? fecha2, string criterio = "")
        {
            TableProductosYAgencia();
            var datos = repoBienes.GetBienesByfechas(criterio, fecha1, fecha2);
            //var datos = repoBienes.GetBienesAll(query);
            return View("ListaDeBienes", datos);            
        }

        [HttpGet]
        public ViewResult Create(string TipoDeProducto)
        {
            TableProductosYAgencia();
            if (TipoDeProducto == "Muebles")
            {
                return View("RegistroDeMuebles");
            }
            else if (TipoDeProducto == "Computo")
            {
                return View("RegistrarProducComputo");
            }
            else 
            {
                return View("RegistroDeVehiculos");
            }
            //return View("RegistrarBienes");
        }

        [HttpPost]
        public ActionResult Create(Bienes bienes)
        {
            string segundaParteCodigo = "";
            if (ModelState.IsValid)
            {
                repoBienes.Store(bienes);

                if (bienes.Idbienes <= 9)
                {
                    segundaParteCodigo = "0" + bienes.Idbienes;
                }else{
                    segundaParteCodigo = "" + bienes.Idbienes+""  ;
                }

                string codigo = bienes.Grupo + "-" + segundaParteCodigo;

                bienes.CodigoGrupo = codigo;

                repoBienes.Update(bienes);
                TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                return RedirectToAction("Index");
            }
            TableProductosYAgencia();

            return View("RegistrarBienes", bienes);
        }

        [HttpGet]
        public ViewResult Edit(Int32 id)
        {
            var data = repoBienes.Find(id);

            //var producto = repoProducto.AllProducto("");
            //ViewData["IdProductofk"] = new SelectList(producto, "IdProducto", "Nombre", data.IdProductoFk);

            var agencia = repoAgencia.All();
            ViewData["IdAgencia"] = new SelectList(agencia, "IdAgencia", "Nombre", data.IdAgencia);

            var area = repoArea.AllArea("");
            ViewData["IdAreafk"] = new SelectList(area, "IdArea", "Nombre", data.IdAreafk);

            var depreciacion = repoDepreciacion.AllDepreciacion("");
            ViewData["IdDepreciacionFk"] = new SelectList(depreciacion, "IdDepreciacion", "Porcentaje");


            return View("EditarTipoBieMuebles", data);
        }

        [HttpPost]
        public ActionResult Edit(Bienes bienes)
        {
            repoBienes.Update(bienes);
            TempData["UpdateSuccess"] = "Actualizado Correctamente";
            return RedirectToAction("Index");
        }


    }
}
