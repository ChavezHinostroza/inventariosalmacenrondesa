﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Interfaces;
using Validator;
using DataBase;
using Repository;

namespace SGRondesa.Controllers
{
    public class AgenciaController : Controller
    {
        private InterfaceAgencia repoAgencia;
        private InterfaceArea repoAreas;
        private InterfaceDetalleAgencia detalleAgencia;

        public AgenciaController(AgenciaRepository repoAgencia, AreaRepository repoAreas, DetalleAgenciaRepository detalleAgencia)
        {
            this.repoAgencia = repoAgencia;
            this.repoAreas = repoAreas;
            this.detalleAgencia = detalleAgencia;
        }

        [HttpGet]
        public ViewResult Index(string criterio="")
        {
            var datos = repoAgencia.ByQueryAll(criterio);
            return View("Inicio", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            ViewBag.Area = repoAreas.AllArea("");
            return View("RegistrarAgencia");

        }

        [HttpPost]
        public ActionResult Create(Agencia agencia)
        {
            if (agencia == null)
            {
                agencia = new Agencia();
            }
            if (ModelState.IsValid)
            {
                repoAgencia.Store(agencia);
                TempData["UpdateSuccess"] = "Se Guardo Correctmente";
                return RedirectToAction("Index");
            }
            return View("RegistrarAgencia", agencia);
        }

        [HttpGet]
        public ActionResult Item(int id, int position)
        {
            var model = repoAreas.Find(id);
            ViewBag.Area = position;
            return PartialView("_Item", model);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repoAgencia.Find(id);
            ViewBag.Detalle = detalleAgencia.GetDetalleByNro(data.Nombre);
            //ViewBag.Area = repoAreas.All();
            return View("Editar", data);
        }

        [HttpPost]
        public ActionResult Edit(Agencia agencia)
        {
            repoAgencia.Update(agencia);
            TempData["UpdateSuccess"] = "Se Actualizo correctamente";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repoAgencia.Delete(id);
            TempData["UpdateSuccess"] = "Se Elimino correctamente";
            return RedirectToAction("Index");
        }
    }
}
