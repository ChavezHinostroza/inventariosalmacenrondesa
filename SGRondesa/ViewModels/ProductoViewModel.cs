﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace SGRondesa.ViewModels
{
    public class ProductoViewModel
    {
        public Int32 IdProducto { get; set; }
        public string Nombre { get; set; }
        public string Modelo { get; set; }
        public string Serie { get; set; }
        public string Marca { get; set; }
        // public decimal Stock { get; set; }
        public string Color { get; set; }

        public Int32 Cantidad { get; set; }
        public string UnidadMedida { get; set; }
        public string Medidas { get; set; }
        public string NumMotor { get; set; }
        public string Chasis { get; set; }
        public string TipoDeProducto { get; set; }
        public string Talla { get; set; }
        public string Sexo { get; set; }
        public double Precio { get; set; }
        public Int32 NumFactura { get; set; }
        public string Observaciones { get; set; }

        public string AreaM2 { get; set; }
        public string Dimenciones { get; set; }
        public string Ubicacion { get; set; }

        public DateTime FechaIngreso { get; set; }
        public DateTime FechaVencimiento { get; set; }

        public Int32 IdProveedorFk { get; set; }
        public Proveedor Proveedor { get; set; }

        public List<ProductoViewModel> ProductoViewModels { get; set; }

        public Int32 ProductoSeleccionadoId { get; set; }
        public Int32 CantidadProductoSeleccionado { get; set; }

        public string Action { get; set; }
    }
}