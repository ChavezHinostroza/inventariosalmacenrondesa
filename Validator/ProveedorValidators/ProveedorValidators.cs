﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Modelos
using Entities;

namespace Validator.ProveedorValidators
{
   public class ProveedorValidators
    {
       public virtual bool Pass(Proveedor proveedor)
       {
           if (String.IsNullOrEmpty(proveedor.Representante))
               return false;
           if (String.IsNullOrEmpty(proveedor.Correo))
               return false;
           if (String.IsNullOrEmpty(proveedor.Telefono))
               return false;
           if (String.IsNullOrEmpty(proveedor.TelefonoOp))
               return false;
           if (String.IsNullOrEmpty(proveedor.RazonSocial))
               return false;
           if (String.IsNullOrEmpty(proveedor.RUC))
               return false;
           if (String.IsNullOrEmpty(proveedor.Direccion))
               return false;
          
          
           return true;
       }
    }
}
