﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
   public class Depreciacion
    {
        public Int32 IdDepreciacion { get; set; }
        public string Descripcion { get; set; }
        public Int32 Porcentaje { get; set; }
        //public decimal DepreciacionMes { get; set; }
        //public decimal DepreciacionAcumulada { get; set; }
        //public decimal ValorNeto { get; set; }

    }
}
